﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Challenge1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IView
    {
        Presenter presenter;

        public MainWindow()
        {
            InitializeComponent();
            presenter = new Presenter(this);
            TypeAnimalComboBox.SelectedIndex = 0;
        }

        private void NextAnimalButton_Click(object sender, RoutedEventArgs e)
        {
            presenter.NextAnimal();
        }

        private void PreviousAnimalButton_Click(object sender, RoutedEventArgs e)
        {
            presenter.PreviousAnimal();
        }

        private void DeleteAnimalButton_Click(object sender, RoutedEventArgs e)
        {
            if (!presenter.RemoveAnimal())
            {
                MessageBox.Show("Удаление невозможно");
            }
        }

        private void AddAnimalButton_Click(object sender, RoutedEventArgs e)
        {
            if (!presenter.AddAnimal())
            {
                MessageBox.Show("Введите данные о животном");
            }
        }

        private void SaveToTxtButton_Click(object sender, RoutedEventArgs e)
        {
            presenter.SaveToTxt();
        }

        private void SaveToXmlButton_Click(object sender, RoutedEventArgs e)
        {
            presenter.SaveToXml();
        }

        public List<string> TypesRuAnimal { set => TypeAnimalComboBox.ItemsSource = value; }
        public string TypeAnimal { set => TypeAnimalLabel.Content = value; }
        public string NicknameAnimal { set => NicknameLabel.Content = value; }
        public string OwnerAnimal { set => OwnerLabel.Content = value; }
        public string TypeForAddAnimal { get => UknownTypeTextBox.Text; }
        public string NicknameForAddAnimal { get => NicknameTextBox.Text; }
        public string OwnerForAddAnimal { get => OwnerTextBox.Text; }

        private void TypeAnimalComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UknownTypeTextBox.IsEnabled = presenter.IsUknownAnimal(TypeAnimalComboBox.SelectedValue.ToString());
            UknownTypeTextBox.Text = TypeAnimalComboBox.SelectedValue.ToString();
        }
    }
}
