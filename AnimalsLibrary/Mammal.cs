﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class Mammal : IAnimal
    {
        public string Nickname { get; set; }
        public string Owner { get; set; }
        public string Type { get; set; }

        public Mammal(string nickname, string owner)
        {
            Nickname = nickname;
            Owner = owner;
            Type = "Mammal";
        }
    }
}
