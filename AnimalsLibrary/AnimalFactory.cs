﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    static class AnimalFactory
    {
        public static IAnimal GetAnimal(string animalType, string nickname, string owner)
        {
            switch (animalType)
            {
                case "Mammal":
                    return new Mammal(nickname, owner);
                case "Bird":
                    return new Bird(nickname, owner);
                case "Amphibian":
                    return new Amphibian(nickname, owner);
                default:
                    return new UknownAnimal(nickname, owner, animalType);
            }
        }
    }
}
