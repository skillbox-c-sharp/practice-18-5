﻿using System.Xml.Linq;

namespace Challenge1
{
    internal class KeeperXml : ISaver
    {
        private string FileName { get; set; }

        public KeeperXml(string fileName)
        {
            FileName = fileName;
        }

        public void Save(Repository repository)
        {
            XElement xAnimals = new XElement("Animals");
            for (int i = 0; i < repository.Count; i++)
            {
                XElement animalElement = new XElement("Animal",
                    new XElement("Type", repository[i].Type),
                    new XElement("Nickname", repository[i].Nickname),
                    new XElement("Owner", repository[i].Owner));
                xAnimals.Add(animalElement);
            }
            xAnimals.Save($"{FileName}.xml");

        }
    }
}
