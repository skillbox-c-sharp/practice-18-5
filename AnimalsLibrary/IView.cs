﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public interface IView
    {
        public List<string> TypesRuAnimal { set; }
        public string TypeAnimal { set; }
        public string NicknameAnimal { set; }
        public string OwnerAnimal { set; }
        public string TypeForAddAnimal { get; }
        public string NicknameForAddAnimal { get; }
        public string OwnerForAddAnimal { get; }

    }
}
