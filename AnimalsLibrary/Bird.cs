﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class Bird : IAnimal
    {
        public string Nickname { get; set; }
        public string Owner { get; set; }
        public string Type { get; set; }

        public Bird(string nickname, string owner)
        {
            Nickname = nickname;
            Owner = owner;
            Type = "Bird";
        }
    }
}
