﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Challenge1
{
    public class Presenter
    {
        IView view;
        int currentId;
        Repository repository;

        public Presenter(IView view)
        {
            this.view = view;
            repository = RepositoryFactory.GetRepository(10);
            Prepare();
        }

        private void Prepare()
        {
            currentId = 0;
            view.TypesRuAnimal = Repository.typesRuAnimal;
            ShowAnimalInfo();
        }

        private void CheckId()
        {
            if (currentId > repository.Count - 1) currentId = 0;
            else if (currentId < 0) currentId = repository.Count - 1;
        }

        private void ShowAnimalInfo()
        {
            CheckId();
            view.TypeAnimal = $"Тип: {Repository.GetRuTypeOfAnimal(repository[currentId].Type)}";
            view.NicknameAnimal = $"Кличка: {repository[currentId].Nickname}";
            view.OwnerAnimal = $"Владелец: {repository[currentId].Owner}";
        }

        public void NextAnimal()
        {
            currentId++;
            ShowAnimalInfo();
        }

        public void PreviousAnimal()
        {
            currentId--;
            ShowAnimalInfo();
        }

        public bool RemoveAnimal()
        {
            bool isSuccess = repository.Remove(currentId);
            if (isSuccess) ShowAnimalInfo();
            return isSuccess;
        }

        public bool AddAnimal()
        {
            string nickname = view.NicknameForAddAnimal;
            string owner = view.OwnerForAddAnimal;
            bool isSuccess;
            if (string.IsNullOrEmpty(owner) || string.IsNullOrEmpty(nickname))
            {
                isSuccess = false;
            }
            else
            {
                string ruType = view.TypeForAddAnimal;
                if (IsUknownAnimal(ruType))
                {
                    repository.Add(AnimalFactory.GetAnimal(ruType, nickname, owner));
                }
                else
                {
                    string type = Repository.GetTypeOfAnimal(ruType);
                    repository.Add(AnimalFactory.GetAnimal(type, nickname, owner));
                }
                isSuccess = true;
            }
            return isSuccess;
        }

        public void SaveToTxt()
        {
            var saveToTxt = new KeeperTxt("filename");
            RepositorySaver rs = new RepositorySaver(saveToTxt);
            rs.Save(repository);
        }

        public void SaveToXml()
        {
            var saveToXml = new KeeperXml("filename");
            RepositorySaver rs = new RepositorySaver(saveToXml);
            rs.Save(repository);
        }

        public bool IsUknownAnimal(string ruTypeAnimal)
        {
            if (Repository.GetTypeOfAnimal(ruTypeAnimal).Equals("UknownAnimal")) return true;
            return false;
        }
    }
}
