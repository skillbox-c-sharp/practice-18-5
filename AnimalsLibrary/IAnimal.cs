﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    interface IAnimal
    {
        public string Nickname { get; set; }
        public string Owner { get; set; }
        public string Type { get; set; }
    }
}
