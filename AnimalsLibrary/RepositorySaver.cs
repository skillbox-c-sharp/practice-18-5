﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class RepositorySaver
    {
        public ISaver SaveMode { get; set; }

        public RepositorySaver(ISaver saveMode) 
        {
            SaveMode = saveMode;
        }

        public void Save(Repository repository)
        {
            SaveMode.Save(repository);
        }
    }
}
