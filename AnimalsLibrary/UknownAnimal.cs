﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class UknownAnimal : IAnimal
    {
        public string Nickname { get; set; }
        public string Owner { get; set; }
        public string Type { get; set; }

        public UknownAnimal(string nickname, string owner, string type)
        {
            Nickname = nickname;
            Owner = owner;
            Type = type;
        }
    }
}
