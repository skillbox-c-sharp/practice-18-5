﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    interface ISaver
    {
        public void Save(Repository repository);
    }
}
