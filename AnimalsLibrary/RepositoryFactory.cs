﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    static class RepositoryFactory
    {
        static Random r;

        static RepositoryFactory()
        {
            r = new Random();
        }

        public static Repository GetRepository(int count)
        {
            List<IAnimal> temp = new List<IAnimal>();

            for (int i = 0; i < count; i++)
            {
                switch (r.Next(3))
                {
                    case 0:
                        temp.Add(new Mammal($"Кличка {i}", $"Хозяин {i}"));
                        break;
                    case 1:
                        temp.Add(new Bird($"Кличка {i}", $"Хозяин {i}"));
                        break;
                    default:
                        temp.Add(new Amphibian($"Кличка {i}", $"Хозяин {i}"));
                        break;
                }
            }
            return new Repository(temp);
        }
    }
}
