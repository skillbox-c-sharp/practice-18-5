﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class KeeperTxt : ISaver
    {
        private string FileName { get; set; }

        public KeeperTxt(string fileName)
        {
            FileName = fileName;
        }

        public void Save(Repository repository)
        {
            using (StreamWriter sw = new StreamWriter($"{FileName}.txt"))
            {
                for (int i = 0; i < repository.Count; i++)
                {
                    sw.WriteLine($"{repository[i].Type} {repository[i].Nickname} {repository[i].Owner}");
                }
            }
        }
    }
}
