﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    class Repository : IEnumerable
    {
        public static List<string> typesAnimal;
        public static List<string> typesRuAnimal;

        private List<IAnimal> db;

        static Repository()
        {
            typesAnimal = new List<string>
            {
                "Mammal",
                "Bird",
                "Amphibian",
                "UknownAnimal"
            };
            typesRuAnimal = new List<string>
            {
                "Млекопитающее",
                "Птица",
                "Земноводное",
                "Неизвестный вид"
            };
        }

        public Repository()
        {
            db = new List<IAnimal>();
        }

        public Repository(List<IAnimal> animals)
        {
            db = animals;
        }

        public static string GetRuTypeOfAnimal(string typeOfAnimal)
        {
            int typeId = typesAnimal.IndexOf(typeOfAnimal);
            if (typeId != -1) return typesRuAnimal[typeId];
            else return typeOfAnimal;

        }

        public static string GetTypeOfAnimal(string ruTypeOfAnimal)
        {
            int typeId = typesRuAnimal.IndexOf(ruTypeOfAnimal);
            if (typeId != -1) return typesAnimal[typeId];
            else return ruTypeOfAnimal;
        }

        public int Count => db.Count;

        public void Add(IAnimal animal)
        {
            db.Add(animal);
        }

        public IAnimal this[int id]
        {
            get => db[id];
        }

        public bool Remove(int id)
        {
            if(Count > 1)
            {
                db.Remove(db[id]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return db.GetEnumerator();
        }
    }
}
